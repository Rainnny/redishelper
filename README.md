# RedisHelper

This is not a plugin! This is a simple Redis API which has a built in packet system.
An example use would be:
Having a network-wide broadcast command for your Minecraft server

**To use this API, you must shade it into your program or plugin**

**Output Example**
*See example package (me.rainnny.example) to see the code*

```
Connected to Redis, type a message to get started!
Hey, I'm Rainnny!
MessagePacket was sent: 1588397977075
[MESSAGE] Rainnny: Hey, I'm Rainnny!
MessagePacket was received: 1588397977097
```