package me.rainnny.example;

import me.rainnny.example.packet.MessagePacket;
import me.rainnny.redis.RedisHelper;
import me.rainnny.redis.packet.IPacket;
import me.rainnny.redis.packet.PacketAdapter;
import me.rainnny.redis.packet.PacketReceiver;

import java.util.Scanner;

/*
 * Date Created: May 02, 2020
 *
 * @author Braydon
 *
 */
public class Main {
    public static void main(String[] args) {
        // This connects to Redis
        RedisHelper redis = new RedisHelper();
        System.out.println("Connected to Redis, type a message to get started!");

        // We initialize the packet adapter, this allows the receiving of packets to work
        // We also provide our own packet receiver (this is optional, it's just an example)
        PacketAdapter adapter = new PacketAdapter(redis, new PacketReceiver() {
            @Override
            public void onPacketSend(IPacket packet, long timestamp) {
                System.out.println(packet.getClass().getSimpleName() + " was sent: " + timestamp);
            }

            @Override
            public void onPacketReceive(IPacket packet, long timestamp) {
                System.out.println(packet.getClass().getSimpleName() + " was received: " + timestamp);
            }
        });
        // We register our packet so the packet adapter can read it
        adapter.addPacket(MessagePacket.class);

        // As an example, we setup a scanner for the program, so whatever you type in terminal, will be sent across Redis with our packet system!
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.isEmpty())
                System.out.println("You cannot send an empty message!");
            else
                new MessagePacket("Rainnny", line).publish();
        }
    }
}