package me.rainnny.example.packet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.rainnny.redis.packet.IPacket;

/*
 * Date Created: May 02, 2020
 *
 * @author Braydon
 *
 */
@AllArgsConstructor @Getter
public class MessagePacket implements IPacket {
    private final String sender, message;

    @Override
    public void onReceive() {
        System.out.println("[MESSAGE] " + sender + ": " + message);
    }
}