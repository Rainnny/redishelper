package me.rainnny.redis.packet;

/*
 * Date Created: May 02, 2020
 *
 * @author Braydon
 *
 */
public interface PacketReceiver {
    void onPacketSend(IPacket packet, long timestamp);

    void onPacketReceive(IPacket packet, long timestamp);
}