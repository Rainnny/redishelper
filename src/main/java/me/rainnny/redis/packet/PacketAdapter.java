package me.rainnny.redis.packet;

import com.google.gson.Gson;
import lombok.Getter;
import me.rainnny.redis.RedisHelper;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.util.ArrayList;
import java.util.List;

/*
 * Date Created: May 02, 2020
 *
 * @author Braydon
 *
 */
public class PacketAdapter {
    @Getter private static PacketAdapter adapter;

    private final RedisHelper helper;
    private final PacketReceiver receiver;
    private final Gson gson = new Gson();

    private final List<Class<? extends IPacket>> packets = new ArrayList<>();

    public PacketAdapter(RedisHelper redisHelper) {
        this(redisHelper, null);
    }

    public PacketAdapter(RedisHelper redisHelper, PacketReceiver receiver) {
        helper = redisHelper;
        this.receiver = receiver;
        adapter = this;

        // Starts listening for incoming jedis messages with the channel pattern: RedisHelperPacket:*
        new Thread(() -> {
            Jedis jedis = null;
            try {
                jedis = helper.getPool().getResource();

                jedis.psubscribe(new JedisPubSub() {
                    @Override
                    public void onPMessage(String pattern, String channel, String message) {
                        // Checks if the incoming message is from a valid (registered) packet
                        Class<? extends IPacket> clazz = packets.stream().filter(packet -> packet.getSimpleName().equals(channel.split(":")[1]))
                                .findFirst().orElse(null);
                        // If the "clazz" variable is null, the packet wasn't identified
                        if (clazz == null)
                            return;
                        IPacket packet = gson.fromJson(message, clazz);
                        // If the "packet" variable is null, there was a problem getting the object from the json, and we wanna return
                        if (packet == null)
                            return;
                        packet.onReceive();
                        if (receiver != null)
                            receiver.onPacketReceive(packet, System.currentTimeMillis());
                    }
                }, "RedisHelperPacket:*");
            } catch (JedisConnectionException ex) {
                helper.getPool().returnBrokenResource(jedis);
                jedis = null;
                ex.printStackTrace();
            } finally {
                if (jedis != null)
                    helper.getPool().returnResource(jedis);
            }

        }, "RedisHelper - Packet Adapter Receiver").start();
    }

    public void addPacket(Class<? extends IPacket> packetClass) {
        packets.add(packetClass);
    }

    public void publishPacket(IPacket packet) {
        new Thread(() -> {
            Jedis jedis = null;
            try {
                jedis = helper.getPool().getResource();

                // Sends the IPacket object as a json across Redis on the channel: RedisHelperPacket:CLASSNAME
                jedis.publish("RedisHelperPacket:" + packet.getClass().getSimpleName(), gson.toJson(packet));
                if (receiver != null)
                    receiver.onPacketSend(packet, System.currentTimeMillis());
            } catch (JedisConnectionException ex) {
                helper.getPool().returnBrokenResource(jedis);
                ex.printStackTrace();
            } finally {
                if (jedis != null)
                    helper.getPool().returnResource(jedis);
            }
        }).start();
    }
}