package me.rainnny.redis.packet;

/*
 * Date Created: May 02, 2020
 *
 * @author Braydon
 *
 */
public interface IPacket {
    void onReceive();

    default void publish() {
        PacketAdapter.getAdapter().publishPacket(this);
    }
}