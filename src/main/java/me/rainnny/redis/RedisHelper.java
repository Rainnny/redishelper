package me.rainnny.redis;

import lombok.Getter;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

/*
 * Date Created: May 02, 2020
 *
 * @author Braydon
 *
 */
@Getter
public class RedisHelper {
    private final JedisPool pool;
    
    public RedisHelper() {
        this("localhost", 6379, null);
    }

    public RedisHelper(String host) {
        this(host, 6379, null);
    }

    public RedisHelper(String host, int port) {
        this(host, port, null);
    }

    public RedisHelper(String host, int port, String password) {
        // Creating the connection pool
        pool = new JedisPool(new JedisPoolConfig(), host, port, Protocol.DEFAULT_TIMEOUT, password);
    }
}